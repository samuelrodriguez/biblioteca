import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

exports.sendReservasNotification = functions.database.ref('/Reservas/{reservaId}/')
    .onWrite(async (change, context) => {
        const reservaId = context.params.reservaId;
        // If un-follow we exit the function.
        /*if (!change.after.val()) {
            return console.log('User ', reservaId);
        }*/
        console.log('We have a new follower UID:', reservaId);

        // Get the list of device notification tokens.
        const getDeviceTokensPromise = admin.database()
            .ref(`/Reservas/${reservaId}/estado`).once('value');

        const result = await Promise.all([getDeviceTokensPromise]);

        if(result[0].val()==="Recoger"){
            console.log("Penalización: ", result[0].val());
            const usuario = admin.database().ref(`/Reservas/${reservaId}/usuario`).once('value');
            const UsuarioResultado = await Promise.all([usuario]);
            const extraerToken = admin.database().ref(`/Usuarios/${UsuarioResultado[0].val()}/token`).once('value');
            const token = await Promise.all([extraerToken]);
            console.log("token:", token[0].val());

            const titulo = admin.database().ref(`/Reservas/${reservaId}/titulo`).once('value');
            const title = await Promise.all([titulo]);
            const payload = {
                notification: {
                    title: 'Ya puedes pasar a recoger el libro:',
                    body: title[0].val(),
                    sound: "default",
                }
            };
            console.log("payload:",payload);
            const response = await admin.messaging().sendToDevice(token[0].val(), payload);

            response.results.forEach((resulta, index) => {
                console.log("ERROR",resulta.error);
            });
        }else{
            console.log("lo cancele:", result[0].val());
        }
    });


exports.sendPenalizacionNotification = functions.database.ref('/Usuarios/{usuarioid}/')
    .onWrite(async (change, context) => {
        const usuarioid = context.params.usuarioid;
        // If un-follow we exit the function.
        /*if (!change.after.val()) {
            return console.log('User ', reservaId);
        }*/
        console.log('We have a new follower UID:', usuarioid);

        // Get the list of device notification tokens.
        const getDeviceTokensPromise = admin.database()
            .ref(`/Usuarios/${usuarioid}/penalizacion`).once('value');

        const result = await Promise.all([getDeviceTokensPromise]);

            console.log("Soy el mejor", result[0].val());
            const extraerToken = admin.database().ref(`/Usuarios/${usuarioid}/token`).once('value');
            const token = await Promise.all([extraerToken]);
            console.log("token:", token[0].val());

        const extraernotificado = admin.database().ref(`/Usuarios/${usuarioid}/notificado`).once('value');
        const notificado = await Promise.all([extraernotificado]);

        console.log("token:", token[0].val());
        if(result[0].val()!==0 && notificado[0].val()===0){
            const dia = new Date();
            for(let i=0; i<7*(result[0].val()+1); i++) {
                //dia.setTime( dia.getTime()+24*60*60*1000 );
                dia.setDate(dia.getDate()+1);
                if( (dia.getDay()===6) || (dia.getDay()===0) )
                    i--;
            }
            console.log("fecha: ",dia.toLocaleDateString());
            const payload = {
                notification: {
                    title: 'Actualmente estas penalizado hasta',
                    body: (dia.getDate() + "/" +dia.getMonth() + "/" +dia.getFullYear() ).toString(),
                    sound: "default",
                }
            };
            console.log("payload:",payload);
            const response = await admin.messaging().sendToDevice(token[0].val(), payload);

            response.results.forEach((resulta, index) => {
                console.log("ERROR",resulta.error);
            });
            const despreciable=admin.database().ref(`/Usuarios/${usuarioid}`).update({notificado: 1});
            console.log(despreciable);
        }else if (result[0].val()===0 && notificado[0].val()===0){
            const payload = {
                notification: {
                    title: 'Ya no estas penalizado.',
                    sound: "default",
                }
            };
            console.log("payload:",payload);
            const response = await admin.messaging().sendToDevice(token[0].val(), payload);

            response.results.forEach((resulta, index) => {
                console.log("ERROR",resulta.error);
            });
        }
    });



exports.RenovarAutomatico = functions.https.onRequest(async (req, res) => {
    // ... pro only
    res.send("Comenzamos renovar");
    console.log("Comenzamos renovar");

    const valor = admin.database().ref("prestamos").orderByChild('fechaFin').equalTo('').once('value').then(async (objeto) => {

        for (let recorrido in objeto.val()) {
            console.log("SUPUESTAMENTE:", objeto.val()[recorrido]);


            let dia = new Date(objeto.val()[recorrido]['fechaIni']);
            for(let i=0; i<7*(objeto.val()[recorrido]['renovaciones']+1); i++) {
                //dia.setTime( dia.getTime()+24*60*60*1000 );
                dia.setDate(dia.getDate()+1);
                if( (dia.getDay()===6) || (dia.getDay()===0) )
                    i--;
            }

            dia.setHours(0);
            dia.setMinutes(0);
            dia.setSeconds(0);
            let hoy = new Date();
            hoy.setHours(0);
            hoy.setMinutes(0);
            hoy.setSeconds(0);
            let fechaf = dia.getTime()- hoy.getTime();
            let dias =(fechaf<0)? 0:new Date(fechaf).getDate();
            console.log("dias:",dias);
            if(dias<10 && objeto.val()[recorrido]['reservado']!==1){


                const extraerToken = admin.database().ref(`/Usuarios/${objeto.val()[recorrido]['usuario']}/token`).once('value');
                const token = await Promise.all([extraerToken]);
                console.log("token:", token[0].val());
                console.log("ENTRAMOS:",dias);
                const extraerLibro = admin.database().ref(`/Libros/${objeto.val()[recorrido]['idlibro']}/title`).once('value');
                const libro = await Promise.all([extraerLibro]);
                console.log(libro[0].val());
                let payload;
                if(objeto.val()[recorrido]['notificacion']===0) {
                    payload = {
                        notification: {
                            title: 'No te olvides de renovar el libro:',
                            body: libro[0].val(),
                            sound: "default",
                        }
                    };
                    const despreciable= admin.database().ref("prestamos/"+recorrido).update({notificacion: 1});
                    console.log(despreciable);
                    console.log(payload);
                    const response = await admin.messaging().sendToDevice(token[0].val(), payload);
                    console.log(response);
                }else if(objeto.val()[recorrido]['notificacion']===1 && dias===0){
                    payload = {
                        notification: {
                            title: 'No te olvides que hoy tienes que devolver el libro:',
                            body: libro[0].val(),
                            sound: "default",
                        }
                    };

                    console.log(payload);
                    const response = await admin.messaging().sendToDevice(token[0].val(), payload);
                    console.log(response);
                    console.log("Mensaje Enviado");
                }
            }
        }


    });
    console.log(valor);


});



