import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FirebaseService} from '../services/firebase.service';
import * as _ from 'lodash';
import {Observable} from 'rxjs';
import {Events, LoadingController, NavController} from '@ionic/angular';
import {global} from '../global';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.page.html',
  styleUrls: ['./historial.page.scss'],
})
export class HistorialPage implements OnInit {

  global=global.penalizacion;
  public informacion: any=[];

  constructor(private fire: FirebaseService, private load:LoadingController,private nav:NavController,private events:Events, private def: ChangeDetectorRef) {

  }

  ngOnInit() {

    this.events.subscribe('user:loggedin',()=>{
      this.global=global.penalizacion;
      this.def.detectChanges();
    });

    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{

        this.fire.prestamos().subscribe((res)=>{
          var info =_.chain(res).filter((g)=>g.fechaFin!=="").value();
          for(let item in info){

            this.fire.getLibro(info[item]['idlibro']).subscribe((res)=>{

              info[item]=Object.assign({titulo: res[6], author:res[2],img:res[5]},info[item]);
              console.log(info);
              this.informacion=_.chain(info).map(g=>{
                return {
                  renovacion: g.renovacion,
                  author: g.author,
                  titulo: g.titulo,
                  idlibro: g.idlibro,
                  img: g.img,
                  reservado: g.reservado,
                  fechaFin: g.fechaFin,
                  fechaIni: g.fechaIni,
                  ejemplarid: g.ejemplarid,
                }
              }).value();
            });
          }
          a.dismiss();
        });
      });
    });
  }

  pagina(ruta: any) {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.nav.navigateForward(["/book/",ruta]);
        a.dismiss();
      });
    });
  }

}
