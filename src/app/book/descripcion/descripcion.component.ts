import {Component,  OnInit} from '@angular/core';
import {Events} from '@ionic/angular';
import {BookService} from '../../services/book.service';

@Component({
  selector: 'app-descripcion',
  templateUrl: './descripcion.component.html',
  styleUrls: ['./descripcion.component.scss'],
})
export class DescripcionComponent implements OnInit {
  public text="";
  constructor( private bookService: BookService, private event: Events) {
    this.event.subscribe('book:descrip', ()=>{
      this.text=this.bookService.description;
    });
  }

  ngOnInit() {
    this.text=this.bookService.description;
  }

}
