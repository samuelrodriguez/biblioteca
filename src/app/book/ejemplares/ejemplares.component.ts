import { Component, OnInit } from '@angular/core';
import {BookService} from '../../services/book.service';
import {Events, LoadingController, NavController, ToastController} from '@ionic/angular';
import * as _ from 'lodash';
import {FirebaseService} from '../../services/firebase.service';
import {AuthenticationService} from '../../services/authentication.service';
import {global} from 'src/app/global';

@Component({
  selector: 'app-ejemplares',
  templateUrl: './ejemplares.component.html',
  styleUrls: ['./ejemplares.component.scss']
})
export class EjemplaresComponent implements OnInit {
  public ejemplares;
  public prestados;
  public disponibles;
  public reservado;
  public logeado: boolean=false;
  constructor( private bookService: BookService,private auth: AuthenticationService,private toast:ToastController, private event: Events, private nav: NavController,private db: FirebaseService,public loader: LoadingController) {

    this.logeado= this.auth.isAuthenticated();
    this.event.subscribe('user:loggedin', ()=>{
      this.logeado= true;
    });
    this.event.subscribe('book:ejemplares', ()=>{
      this.ngOnInit();
    });
    this.event.subscribe('book:reservado', ()=>{
      this.ngOnInit();
    });
    this.event.subscribe('book:prestado', ()=>{
      this.ngOnInit();
    });
    this.event.subscribe('book:disponible', ()=>{
      this.ngOnInit(); });
  }

  ngOnInit() {

    this.ejemplares=_.chain(this.bookService.ejemplares)
        .groupBy('estado')
        .toPairs().map(item=>_.zipObject(['estado','info'],item))
        .value();
    console.log(this.ejemplares);
    this.prestados=this.bookService.prestado;
    this.reservado=this.bookService.reservado;
    this.disponibles=this.bookService.disponible;
  }

  reservar() {
    var valor = new Date();
    var valor2 =valor.getTime();
    console.log("Convertida a numero", valor2, "Despues: ", new Date(valor2).toLocaleString());
      console.log(valor.toLocaleString());

    const loader = this.loader.create({
      message: "Cargando..."
    });

    let toast;
   loader.then(a=> {
      a.present().then(()=>{
        var transaction = {
          libroid: this.bookService.argumento,
          estado: "pendiente",
          titulo: this.bookService.title,
          fecha: new Date().getTime(),
          usuario: this.auth.id
        };
        this.db.getReservaUID(this.auth.id).subscribe(async (item)=>{
          this.db.prestamos().subscribe(async (valor) => {

            var result = 0;
            if (_.chain(valor).filter(g => g.idlibro === this.bookService.argumento).value().length != 0) {
              result = 1;
              toast = await this.toast.create({
                message: 'Usted ya tiene este libro, no puedes reservarlo.',
                duration: 2000
              });
              toast.present();
            }
            for (let objeto in item) {
              if (this.bookService.argumento === item[objeto]['libroid'] && item[objeto]['estado'] != "Cancelado") {
                result = 1;
                toast = await this.toast.create({
                  message: 'Usted ya tiene una reserva de este libro.',
                  duration: 2000
                });
                toast.present();
              }

            }
            if (result == 0) {
              this.db.addTransaction(transaction, 'Reservas');
              this.db.comprobarDisponibilidad(this.bookService.argumento);
              this.ngOnInit();
            }
          });
        });

        a.dismiss();
      });
    });

  }
}
