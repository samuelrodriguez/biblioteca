import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {BookService} from '../../services/book.service';
import {AlertController, LoadingController, ModalController} from '@ionic/angular';
import {AuthenticationService} from '../../services/authentication.service';
import {ModalPage} from '../modal/modal.page';

@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.component.html',
  styleUrls: ['./comentarios.component.scss']
})
export class ComentariosComponent implements OnInit{
  public comentarios;
  logeado: boolean=false;
  constructor(private fire: FirebaseService,public modalCtrl:ModalController,private def:ChangeDetectorRef, private book: BookService,private auth: AuthenticationService, private loader: LoadingController,public alertController: AlertController) {

  }

  ngOnInit() {
    this.comentarios=this.fire.getComentarios(this.book.argumento);
    this.logeado=this.auth.isAuthenticated();
  }

  async presentAlert(name: string, text: string) {

    const alert = await this.alertController.create({
      header: name,
      message: "<label class='justificate'>"+text+"</label>",
      buttons:['Cerrar']
    });
    return await alert.present();

  }/*
  async comentar(){
    console.log("He entrado Guapo");
    const profileModal = await this.modalCtrl.create({component: ModalPage});
    await profileModal.present();
}*/

  async comentar(){
    const alert = await this.alertController.create({
      inputs: [
        {
          name: 'texto',
          type: 'text',
          placeholder: 'Comentario...'
        }],
      message: "Indroduzca el comentario que desee:",
      buttons:[{text:'Aceptar',handler:(valor)=>{this.comentado(valor)}}]
    });
    return await alert.present();

  }

  comentado(valor) {
    if(valor.texto  === ""){
      return;
    }
    console.log(valor.texto ==="");
    const loader = this.loader.create({
      message: "Cargando..."
    });

    loader.then(a=> {
      a.present().then(()=> {
        var transaction = {
          libroid: this.book.argumento,
          comentario: valor.texto,
          usuario: this.auth.id
        };
        this.fire.addTransaction(transaction,'Comentarios');
        this.ngOnInit();
        this.def.detectChanges();
        a.dismiss();
      });
    });
  }
}
