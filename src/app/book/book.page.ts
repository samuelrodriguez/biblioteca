import {Component, OnInit} from '@angular/core';
import {FirebaseService} from '../services/firebase.service';
import {ActivatedRoute} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {BookService} from '../services/book.service';
import {ActionSheetController, AlertController, Events, LoadingController} from '@ionic/angular';
import {global} from '../global';

@Component({
  selector: 'app-book',
  templateUrl: './book.page.html',
  styleUrls: ['./book.page.scss'],
})
export class BookPage implements OnInit {
  global=global.penalizacion;

  public img;
  public title;
  public argumento;
  public disponible=0;
  public ejemplares;
  public prestado=0;
  public reservado=0;
  public buttons: Array<any>;
  logeado: boolean=false;
  constructor(private fire: FirebaseService,public alertController: AlertController,private loader:LoadingController
              ,public actionSheetController: ActionSheetController,private activeRoute: ActivatedRoute,private events:Events
              , public authService: AuthenticationService,private bookService: BookService) {
    this.fire.getListas().subscribe((value)=> {
      this.createButtons(value);
    });

  }
  ngOnInit() {

    this.argumento = this.activeRoute.snapshot.paramMap.get('id');
    this.logeado=this.authService.isAuthenticated();

    this.bookService.argumento=this.argumento;

    this.fire.getLibro(this.argumento).subscribe(resp => {
      this.bookService.editorial = resp[1];
      this.bookService.edicion = resp[0];
      this.bookService.author = resp[2];
      this.bookService.categoria=resp[3];
      this.bookService.description = resp[4];
      this.img =resp[5];
      this.bookService.title = resp[6];
      this.title = resp[6];
    });
    this.fire.getEjemplaresItems(this.argumento).subscribe((rest) => {
      this.ejemplares=rest;
      this.bookService.ejemplares=this.fire.extraerKeyeInfo(rest);

      for (let restKey in rest) {
        if (rest[restKey]['estado']==='Disponible'){ this.disponible += 1;}
        if (rest[restKey]['estado']==='Prestado'){ this.prestado += 1;}
        if (rest[restKey]['estado']==='Reservado'){ this.reservado += 1;}
      }

      this.bookService.disponible=this.disponible;
      this.bookService.prestado=this.prestado;
      this.bookService.reservado=this.reservado;
    });
  }

  async presentActionSheet() {
    console.log(this.buttons);
    const actionSheet = await this.actionSheetController.create({


      header: 'Mis Listas',
      buttons: this.buttons
    });
    await actionSheet.present();
  }

  createButtons(value) {
    let buttons = [];
    for (let index in value) {
      let button = {
        text: index,
        icon: 'list',
        handler: () => {
          console.log('Añadir '+index);
          this.pushList(index);
        }
      };
      buttons.push(button);
    }
    buttons.push({
      text: 'Añadir nueva lista',
      icon: 'add',
      handler: () => {
        this.newList();
      }
    },{
      text: 'Cancelar',
      icon: 'close',
      role: 'cancel',
      handler: () => {
      }
    });
    this.buttons=buttons;
    return buttons;
  }

  private async newList() {

    const alert = await this.alertController.create({
      inputs: [
        {
          name: 'texto',
          type: 'text',
          placeholder: 'Nombre...'
        }],
      message: 'Indroduzca el nombre de la nueva lista:',
      buttons:[{text:'Aceptar',handler:(valor)=>{this.pushList(valor.texto);}}]
    });

    console.log('nuevoBotonAñadido');
    return await alert.present();
  }

  pushList(valor) {
    if(valor  === ''){
      return;
    }
    console.log(valor.texto ==='');
    const loader = this.loader.create({
      message: 'Cargando...'
    });

    loader.then(a=> {
      a.present().then(()=> {
        this.fire.addTransaction({titulo: this.title, libroid: this.argumento},'Listas/'+this.authService.id+'/'+valor);
      });
      a.dismiss();
      this.events.publish('lista:anadido');
    });
  }
}

