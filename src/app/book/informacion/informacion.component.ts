import { Component, OnInit } from '@angular/core';
import {BookService} from '../../services/book.service';
import {Events} from '@ionic/angular';

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.component.html',
  styleUrls: ['./informacion.component.scss']
})
export class InformacionComponent implements OnInit {
  public author;
  public categori;
  public id;
  public edicion;
  public editorial;
  constructor( private bookService: BookService, private event: Events) {

    this.event.subscribe('book:author', ()=>{
      console.log("entre");
      this.author=this.bookService.author;
    });

    this.event.subscribe('book:categoria', ()=>{
      this.categori=this.bookService.categoria;
    });

    this.event.subscribe('book:argumento', ()=>{
      this.id=this.bookService.argumento;
    });
    this.event.subscribe('book:edicion', ()=>{
      this.edicion=this.bookService.edicion;
    });
    this.event.subscribe('book:editorial', ()=>{
      this.editorial=this.bookService.editorial;
    });
  }

  ngOnInit() {
    this.categori=this.bookService.categoria;
    this.id=this.bookService.argumento;
    this.author=this.bookService.author;
    this.editorial=this.bookService.editorial;
    this.edicion=this.bookService.edicion;

  }

}
