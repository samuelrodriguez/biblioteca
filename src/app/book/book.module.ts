import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { BookPage } from './book.page';
import {MatDialogModule, MatTabsModule} from '@angular/material';
import {DescripcionComponent} from './descripcion/descripcion.component';
import { InformacionComponent } from './informacion/informacion.component';
import { EjemplaresComponent } from './ejemplares/ejemplares.component';
import { ComentariosComponent } from './comentarios/comentarios.component';
import {ModalPage} from './modal/modal.page';


const routes: Routes = [
  {
    path: 'book',
    component: BookPage,
    children: [
      {
        path: 'description',
        component: DescripcionComponent,

      },
      {
        path: 'informacion',
        component: InformacionComponent,

      },
      {
        path: 'comentarios',
        component: ComentariosComponent,

      },
      {
        path: 'ejemplares',
        component: EjemplaresComponent,

      }
    ]
  },
  {
    path: '',
    redirectTo: 'book/description',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    IonicModule,
    MatTabsModule,
    RouterModule.forChild(routes)
  ],
  entryComponents:[ModalPage],
  declarations: [BookPage,DescripcionComponent, InformacionComponent, EjemplaresComponent, ComentariosComponent,ModalPage],
})
export class BookPageModule {}
