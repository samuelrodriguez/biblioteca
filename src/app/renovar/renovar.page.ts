import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AngularFireList} from '@angular/fire/database';
import {AngularFireDatabase} from '@angular/fire/database/database';
import {FirebaseService} from '../services/firebase.service';
import {AlertController, Events, LoadingController, NavController} from '@ionic/angular';
import * as _ from 'lodash';
import {AuthenticationService} from '../services/authentication.service';
import {global} from '../global';

@Component({
  selector: 'app-renovar',
  templateUrl: './renovar.page.html',
  styleUrls: ['./renovar.page.scss'],
})
export class RenovarPage implements OnInit {


  public prestamos:  any=[];
  global=global.penalizacion;

  constructor(private events:Events, private def: ChangeDetectorRef,private db: AngularFireDatabase,private change: ChangeDetectorRef,private nav: NavController, private fire: FirebaseService, private load: LoadingController,private alertController: AlertController,private auth: AuthenticationService) {

  }


  ngOnInit() {
    global.busqueda=true;
    this.events.publish('user:busqueda');

    this.events.subscribe('user:loggedin',()=>{
      this.global=global.penalizacion;
      this.def.detectChanges();
    });
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.fire.prestamos().subscribe((res)=>{
          for (let restKey in res) {
            res[restKey]=(Object.assign({prestamoid: restKey},res[restKey]));
          }
          var info=_.chain(res).filter((g)=>g.fechaFin==="").value();

          for(let item in info){
            this.fire.getLibro(info[item]['idlibro']).subscribe((res)=>{
              info[item]['fechaFin']= this.calcularTiempo(7*(info[item]['renovaciones']+1),info[item]['fechaIni']);
              var inicio = new Date();
              inicio.setHours(0);
              inicio.setMinutes(0);
              inicio.setSeconds(0);

              var fin = new Date(info[item]['fechaFin']);
              fin.setHours(0);
              fin.setMinutes(0);
              fin.setSeconds(0);
              let fechadef= new Date(fin.getTime()).getTime()- new Date(inicio.getTime()).getTime();
              this.fire.prestamos().subscribe((objeto)=>{
                info[item]=(Object.assign({titulo: res[6], author:res[2],img:res[5],
                      dias: (fechadef<0)? -1:new Date(fechadef).getDate()},
                    info[item]));
                this.prestamos=_.chain(info).map(g=> {
                  return {
                    renovaciones: g.renovaciones,
                    ejemplarid: g.ejemplarid,
                    fechaFin: g.fechaFin,
                    fechaIni: g.fechaIni,
                    idlibro: g.idlibro,
                    prestamoid: g.prestamoid,
                    reservado: g.reservado,
                    usuario: g.usuario,
                    title: g.titulo,
                    author: g.author,
                    item: item,
                    img: g.img,
                    dias: g.dias,
                  }
                }).value();
                console.log(this.prestamos);

              });
            });
          }
          a.dismiss();
        });
        console.log(this.prestamos);
      });
    });
  }

  pagina(ruta: any) {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.nav.navigateForward(["/book/",ruta]);
        a.dismiss();
      });
    });
  }
  calcularTiempo(NDias, Fechaini) {
    var dia = new Date(Fechaini);
    for(var i=0; i<NDias; i++) {
      //dia.setTime( dia.getTime()+24*60*60*1000 );
      dia.setDate(dia.getDate()+1);
      if( (dia.getDay()==6) || (dia.getDay()==0) )
        i--;
    }
    return dia.getTime();
  }

  async comentar(key){
    const alert = await this.alertController.create({
      header: name,
      inputs: [
        {
          name: 'texto',
          type: 'text',
          placeholder: 'Comentario...'
        }],
      message: "Indroduzca el comentario que desee:",
      buttons:[{text:'Aceptar',handler:(valor)=>{this.comentado(valor,key)}}]
    });
    return await alert.present();

  }

  comentado(valor, key) {
    if(valor.texto  === ""){
      return;
    }
    console.log(valor.texto ==="");
    const loader = this.load.create({
      message: "Cargando..."
    });

    loader.then(a=> {
      a.present().then(()=> {
        var transaction = {
          libroid: key,
          comentario: valor.texto,
          usuario: this.auth.id
        };
        this.fire.addTransaction(transaction,'Comentarios')
      });
      a.dismiss();
    });
  }

  renovar(key,renovaciones, item) {
    const loader = this.load.create({
      message: "Cargando..."
    });

    loader.then(a=> {
      a.present().then(()=> {
        this.fire.renovar(key,renovaciones);
        console.log();
        this.prestamos[item]['renovaciones']=renovaciones+1;
        this.prestamos[item]['fechaFin']=this.calcularTiempo(7*(this.prestamos[item]['renovaciones']+1),this.prestamos[item]['fechaIni']);
        var inicio = new Date();
        inicio.setHours(0);
        inicio.setMinutes(0);
        inicio.setSeconds(0);
        this.prestamos[item]['dias']=new Date(new Date(this.prestamos[item]['fechaFin']).getTime()- new Date(inicio.getTime()).getTime()).getDate();

        //ARREGLAR FECHAS EN LA LNEA ANTERIOR SON NUMEROS LOS QUE ESTAN GUARDADOS.
        console.log(this.prestamos[item]['dias']);
        this.ngOnInit();
      });
      a.dismiss();
    });

  }
}
