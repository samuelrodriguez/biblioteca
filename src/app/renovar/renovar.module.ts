import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RenovarPage } from './renovar.page';

const routes: Routes = [
  {
    path: '',
    component: RenovarPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RenovarPage],
})
export class RenovarPageModule {}
