import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenovarPage } from './renovar.page';

describe('RenovarPage', () => {
  let component: RenovarPage;
  let fixture: ComponentFixture<RenovarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenovarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenovarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
