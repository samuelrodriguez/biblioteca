import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import * as _ from 'lodash';
import {FirebaseService} from '../services/firebase.service';
import {Events, LoadingController, NavController} from '@ionic/angular';

@Component({
  selector: 'app-renuevo',
  templateUrl: './renovar.component.html',
  styleUrls: ['./renovar.component.scss']
})
export class RenovarComponent implements OnInit {
  private prestamos: any=[];
  slideOpts = {
    slidesPerView: 4,
    initialSlide: 0,
    speed: 400
  };
  constructor(private events:Events,
              private def: ChangeDetectorRef,
              private change: ChangeDetectorRef,
              private nav: NavController,
              private fire: FirebaseService,
              private load: LoadingController) {}
  ngOnInit() {

    const loader = this.load.create({
      message: "Cargando..."
    });

    loader.then(a=> {
      a.present().then(()=> {
        this.fire.prestamos().subscribe((res)=>{
          for (let restKey in res) {
            res[restKey]=(Object.assign({prestamoid: restKey},res[restKey]));
          }
          var info=_.chain(res).filter((g)=>g.fechaFin==="").value();

          for(let item in info){
            this.fire.getLibro(info[item]['idlibro']).subscribe((res)=>{
              info[item]['fechaFin']= this.calcularTiempo(7*(info[item]['renovaciones']+1),info[item]['fechaIni']);
              var inicio = new Date();
              inicio.setHours(0);
              inicio.setMinutes(0);
              inicio.setSeconds(0);

              var fin = new Date(info[item]['fechaFin']);
              fin.setHours(0);
              fin.setMinutes(0);
              fin.setSeconds(0);
              let fechadef= new Date(fin.getTime()).getTime()- new Date(inicio.getTime()).getTime();
              this.fire.prestamos().subscribe((objeto)=>{
                info[item]=(Object.assign({titulo: res[6], author:res[2],img:res[5],
                      dias: (fechadef<0)? -1:new Date(fechadef).getDate()},
                    info[item]));

                this.prestamos=_.chain(info).map(g=> {
                  return {
                    renovaciones: g.renovaciones,
                    ejemplarid: g.ejemplarid,
                    fechaFin: g.fechaFin,
                    fechaIni: g.fechaIni,
                    idlibro: g.idlibro,
                    prestamoid: g.prestamoid,
                    reservado: g.reservado,
                    usuario: g.usuario,
                    title: g.titulo,
                    author: g.author,
                    item: item,
                    img: g.img,
                    dias: g.dias,
                  }
                }).value();

              });
            });
          }
          a.dismiss();
        });
      });
    });
  }
  pagina(ruta: any) {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.nav.navigateForward(["/book/",ruta]);
        a.dismiss();
      });
    });
  }
  calcularTiempo(NDias, Fechaini) {
    var dia = new Date(Fechaini);
    for(var i=0; i<NDias; i++) {
      //dia.setTime( dia.getTime()+24*60*60*1000 );
      dia.setDate(dia.getDate()+1);
      if( (dia.getDay()==6) || (dia.getDay()==0) )
        i--;
    }
    return dia.getTime();
  }
  renovar(key,renovaciones, item) {
    const loader = this.load.create({
      message: "Cargando..."
    });

    loader.then(a=> {
      a.present().then(()=> {
        this.fire.renovar(key,renovaciones);
        this.prestamos[item]['renovaciones']=renovaciones+1;
        this.prestamos[item]['fechaFin']=this.calcularTiempo(7*(this.prestamos[item]['renovaciones']+1),this.prestamos[item]['fechaIni']);
        var inicio = new Date();
        inicio.setHours(0);
        inicio.setMinutes(0);
        inicio.setSeconds(0);
        this.prestamos[item]['dias']=new Date(new Date(this.prestamos[item]['fechaFin']).getTime()- new Date(inicio.getTime()).getTime()).getDate();

        //ARREGLAR FECHAS EN LA LNEA ANTERIOR SON NUMEROS LOS QUE ESTAN GUARDADOS.
        this.ngOnInit();
      });
      a.dismiss();
    });

  }
}
