import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {RouterModule, Routes} from '@angular/router';

import { ListPage } from './list.page';

const routes: Routes = [
  {
    path: 'list',
    component: ListPage,
    children: [
      { path: 'renovar', loadChildren: '../renovar/renovar.module#RenovarPageModule' },
      { path: 'historial', loadChildren: '../historial/historial.module#HistorialPageModule' },
      { path: 'reservas', loadChildren: '../reservas/reservas.module#ReservasPageModule' },
    ]
  },
  {
    path: '',
    redirectTo: 'list/renovar',
    pathMatch: 'full'
  }
  ];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
   /*RouterModule.forChild([
      {
        path: '',
        component: ListPage
      }
    ])*/
  ],
  declarations: [ListPage ],
  exports: [RouterModule]
})
export class ListPageModule {}
