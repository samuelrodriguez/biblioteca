import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from './services/auth-guard.service';
import {LoginService} from './services/login.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule',canActivate:[LoginService]

  },
  {
    path: 'list',
    canActivate: [AuthGuardService],
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule',canActivate:[LoginService]},
  { path: 'settings',
    canActivate: [AuthGuardService],
    loadChildren: './settings/settings.module#SettingsPageModule' },
  { path: 'book/:id', loadChildren: './book/book.module#BookPageModule' },
  { path: 'modal', loadChildren: './book/modal/modal.module#ModalPageModule' },
  { path: 'new-books', loadChildren: './new-books/new-books.module#NewBooksPageModule' ,
    canActivate: [AuthGuardService]},
  { path: 'mylists', loadChildren: './mylists/mylists.module#MylistsPageModule' },
  { path: 'lista-visualizada/:id', loadChildren: './lista-visualizada/lista-visualizada.module#ListaVisualizadaPageModule' },
  { path: 'portada', loadChildren: './portada/portada.module#PortadaPageModule' },




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
