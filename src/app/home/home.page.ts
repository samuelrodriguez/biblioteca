import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {global} from '../global';
import {Events} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  informacion=global;

  constructor(private events:Events, private def: ChangeDetectorRef) {
  }
  ngOnInit(): void {
    this.events.subscribe('user:loggedin',()=>{
      this.def.detectChanges();
    });

  }

}
