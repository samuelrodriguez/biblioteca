import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Events, LoadingController, NavController} from '@ionic/angular';
import {FirebaseService} from '../services/firebase.service';
import {global} from '../global';
import * as _ from 'lodash';

@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.scss']
})
export class ViewListComponent implements OnInit {

  public informacion =[];
  slideOpts = {
    slidesPerView: 4,
    initialSlide: 0,
    speed: 400
  };
  constructor(private events:Events,private fire:FirebaseService, public load:LoadingController,public nav:NavController,private def: ChangeDetectorRef) {
    this.events.subscribe('lista:anadido',(valor)=>{
      console.log('Llegué');
      this.ngOnInit();
    });

  }

  ngOnInit() {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.fire.getListas().subscribe((valor)=>{
          let info =[];
          for (let item in valor){
            this.fire.getListaInfo(item).subscribe((objeto)=>{
              let totalInfo;

              for(let item2 in objeto){
                this.fire.getLibro(objeto[item2]['libroid']).subscribe((value)=>{

                  objeto[item2]=Object.assign(value,{identificador: item2, key:objeto[item2]['libroid']});

                  totalInfo=Object.assign(_.chain(objeto).map(
                      g=>{
                        return {img:g[5],
                          titulo: g[6],
                          key: g.key,
                          identificador: g.identificador
                        }}).value());

                  info[item]= Object.assign({lista: item, open:false,number: _.countBy(valor[item])["[object Object]"],libros: totalInfo});
                  this.informacion.length=0;
                  for(let item in info){
                    this.informacion[this.informacion.length]= info[item];
                  }
                });

              }
            });

          }
          a.dismiss();
        });
      });
    });
  }
  toggleSection(i){
    this.informacion[i].open=!this.informacion[i].open;
  }
  toggleItem(i,j){
    this.informacion[i].children[j].open=!this.informacion[i].open
  }

  pagina(ruta: any) {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.nav.navigateForward(["/book/",ruta]);
        a.dismiss();
      });
    });
  }
}
