import { Component } from '@angular/core';

import {Events, LoadingController, Platform, ToastController} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {AuthenticationService} from './services/authentication.service';
import {Router} from '@angular/router';
import * as firebase from 'firebase';
import {from} from 'rxjs';
import {AngularFireDatabase} from 'angularfire2/database';
import {FcmService} from './services/fcm.service';
import {global} from './global';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  appPage = [
    {
      title: 'Portada / Buscar',
      url: '/home',
      icon: 'home',
      disable: false
    },
    {
      title: 'Identificarse',
      url: '/login',
      icon: 'contact',
      disable: false
    },
  ];
  public name: string='';
  constructor(
      private platform: Platform,
      private splashScreen: SplashScreen,
      private statusBar: StatusBar,
      private events:  Events,
      private router: Router,
      private fdb: AngularFireDatabase,
      public toastController: ToastController,
      public load:LoadingController,
      private authService: AuthenticationService,
      private fcm:FcmService,
  ) {

    const loader = this.load.create({
      message: "Comprobando credenciales..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            events.publish('user:loggedin');
            authService.authenticationState.next(true);
            console.log(user);
            authService.id=firebase.auth().currentUser['uid'];
            events.publish('user:id');
            var uid= fdb.database.ref().child('Usuarios/'+firebase.auth().currentUser['uid']).once('value').then((objeto) => {
              return objeto.val();
            });
            from(uid).subscribe((res)=>{
              for(let item in res){
                authService.name=res[item]['name'];
              }});}
          a.dismiss();
        });
      });
    });

    this.initializeApp();


  }
  private async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 3000
    });
    toast.present();
  }

  private notificationSetup() {
    this.fcm.getToken();
    this.fcm.onNotifications().subscribe(
        (msg) => {
          if (this.platform.is('ios')) {
            this.presentToast(msg.aps.alert);
          } else {
            this.presentToast(msg.body);
          }
        });
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.notificationSetup();
      this.authService.authenticationState.subscribe(state => {
        console.log('Autentificacion cambiada: ', state);
        if (state) {
          this.router.navigate(['portada']);
        } else {
          this.router.navigate(['home']);
        }
      });
    });


    this.events.subscribe('user:loggedin', () => {
      this.name=this.authService.name;
      this.appPage = [
        {title: 'Portada / Buscar',
          url: '/portada',
          icon: 'home',
          disable: false},
        {title: 'Libros',
          url: '/list',
          icon: 'book',
          disable: false},
        {title: 'Mis Listas',
          url: '/mylists',
          disable: false,
          icon: 'list'},
        {title: 'Desideratas',
          url: '/new-books',
          icon: 'basket',
          disable: false},
        {title: 'Desideratas Realizadas',
          disable: true,
          url: '/new-books',
          icon: 'list'},
        {title: 'Ajustes',
          url: '/settings',
          disable: false,
          icon: 'settings'},
        {title: 'Cerrar Sesion',
          disable: false,
          url: '/home',
          icon: 'log-out'}
      ]; });

    this.events.subscribe('user:loggedOut', () => {
      this.name='';
      this.appPage = [
        {title: 'Portada / Buscar',
          disable: false,
          url: '/home',
          icon: 'home'},
        {
          title: 'Identificarse',
          url: '/login',
          disable: false,
          icon: 'contact'
        },
      ];
    });
  }
  logout() {
    this.authService.logout();
  }
}
