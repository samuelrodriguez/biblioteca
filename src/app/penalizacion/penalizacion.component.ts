import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FirebaseService} from '../services/firebase.service';
import {AuthenticationService} from '../services/authentication.service';
import {Events} from '@ionic/angular';
import {global} from '../global';

@Component({
  selector: 'app-penalizacion',
  templateUrl: './penalizacion.component.html',
  styleUrls: ['./penalizacion.component.scss']
})
export class PenalizacionComponent implements OnInit {
  public unidades =0;
  public decimas =0;
  public centesimas =0;
  public total=0;
  public authenticado=false;
  constructor(private fire:FirebaseService,private auth: AuthenticationService,private events:Events,private def: ChangeDetectorRef) {

  }

  ngOnInit() {
    console.log("entre");

      console.log(this.auth.id);
      this.fire.getPenalizacion().subscribe((objeto)=>{
        global.penalizacion=objeto['penalizacion'];
        console.log(objeto['penalizacion']);
        this.events.publish('user:loggedin');
        /*this.total= parseInt(objeto['penalizacion']);
        this.centesimas =  parseInt(objeto['penalizacion'].toString().substr(objeto['penalizacion'].toString().length-3,1));
        console.log(objeto['penalizacion'].toString().substr(objeto['penalizacion'].toString().length-2,1));
        this.decimas =  parseInt(objeto['penalizacion'].toString().substr(objeto['penalizacion'].toString().length-2,1));
        this.unidades = parseInt(objeto['penalizacion'].toString().substr(objeto['penalizacion'].toString().length-1,1));
        this.authenticado=true;
        this.def.detectChanges();*/
      });

  }

}
