import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenalizacionComponent } from './penalizacion.component';

describe('PenalizacionComponent', () => {
  let component: PenalizacionComponent;
  let fixture: ComponentFixture<PenalizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenalizacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenalizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
