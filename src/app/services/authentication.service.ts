import { Injectable } from '@angular/core';
import {BehaviorSubject, from} from 'rxjs';
import {Events, Platform} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {AngularFireAuth} from 'angularfire2/auth';
import {User} from '../login/user';
import {AngularFireDatabase} from 'angularfire2/database';
import * as firebase from 'firebase';

const TOKEN_KEY = 'auth-token';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public id='';
  public name='';
  authenticationState = new BehaviorSubject(false);
  constructor(private afAuth: AngularFireAuth,
              private storage: Storage,
              private plt: Platform,
              private fdb: AngularFireDatabase,
              private events: Events) {
    this.plt.ready().then(() => {
          return this.checkToken();
        }
    );
  }


  logout() {

    this.events.publish('user:loggedOut');

    return this.afAuth.auth.signOut().then( res => {
      this.authenticationState.next(false);
    });
  }

  login(user: User,check:boolean) {
    try {
      console.log(check);
      var val =this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password).then(res => {
        console.log(TOKEN_KEY);
        this.authenticationState.next(true);
        this.events.publish('user:loggedin');

        if(this.authenticationState && check){
          this.persistence(user);
        }
        console.log(firebase.auth().currentUser['uid']);
        this.id= firebase.auth().currentUser['uid'];
        this.events.publish('user:id');
        var uid= this.fdb.database.ref().child('Usuarios/'+this.id).once('value').then((objeto) => {
          return objeto.val();
        });

        from(uid).subscribe((res)=>{
          for(let item in res){
            this.name=res[item]['name'];
          }
        });
      });


      return val;
    } catch (e) {
      console.error(e);
      return e;
    }
  }
  persistence(user){
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
        .then(function() {
          // Existing and future Auth states are now persisted in the current
          // session only. Closing the window would clear any existing state even
          // if a user forgets to sign out.
          // ...
          // New sign-in will be persisted with session persistence.
          console.log(firebase.auth().signInWithEmailAndPassword(user.email, user.password));
          return firebase.auth().signInWithEmailAndPassword(user.email, user.password);
        })
        .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
        });
  }
  isAuthenticated() {
    return this.authenticationState.value;
  }

  checkToken() {
    return this.storage.set(TOKEN_KEY, '').then( res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }
}
