import { Injectable } from '@angular/core';
import {CameraOptions} from '@ionic-native/camera';
import {Camera} from '@ionic-native/camera/ngx';

@Injectable({
  providedIn: 'root'
})
export class CameraService {

  constructor( public camera: Camera) { }
  public imageSrc;
  openGallery () {
    let cameraOptions: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      quality: 100,
      targetWidth: 1000,
      targetHeight: 1000,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true
    };

    return this.camera.getPicture(cameraOptions)
        .then(file_uri => this.imageSrc = file_uri,
            err => console.log(err));
  }
}
