import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AngularFireDatabase} from '@angular/fire/database';
import * as _ from 'lodash';
import {BookService} from './book.service';
import {AuthenticationService} from './authentication.service';
import {Events, ToastController} from '@ionic/angular';
import {global} from '../global';

@Injectable({
    providedIn: 'root'
})
export class FirebaseService {

    constructor(private fdb: AngularFireDatabase, private toast: ToastController, private book: BookService, private auth: AuthenticationService, private event: Events) {
    }

    getLibro(text: string): Observable<any> {
        return this.fdb.list('Libros/' + text).valueChanges();
    }

    getLibros() {

        return this.fdb.list('Libros').snapshotChanges().pipe(
            map(changes => changes.map((c) => {
                return {
                    key: c.payload.key,
                    author: c.payload.val()['author'],
                    img: c.payload.val()['img'],
                    categoria: c.payload.val()['categoria'],
                    title: c.payload.val()['title']
                };
            }))
        );
    }

    getEjemplaresItems(item: string): Observable<any> {
        var val = this.fdb.database.ref().child('Ejemplares/').orderByChild('idlibro').equalTo(item).once('value').then((objeto) => {
            return objeto.val();
        });
        return from(val);
    }

    getComentarios(item: string) {
        //La idea es hacer una nueva llamada dentro del then a usuarios y buscar el usuario y directamente cambiarlo.
        var val = this.fdb.database.ref().child('Comentarios/').orderByChild('libroid').equalTo(item).once('value').then((objeto) => {
            return objeto.val();
        });
        var info: Observable<any>[] = [];
        from(val).subscribe(
            (objeto) => {
                for (let restKey in objeto) {
                    var valor = '';
                    from(this.fdb.database.ref().child('Usuarios/' + objeto[restKey]['usuario']).once('value').then((objeto) =>
                        objeto.val()['name'])
                    ).subscribe((item) =>
                        info.push(Object.assign({comentario: objeto[restKey]['comentario'], usuario: item}))
                    );
                }
            }
        );
        return info;
    }

    extraerKeyeInfo(item: any) {
        var info: Observable<any>[] = [];
        for (let restKey in item) {

            info.push(Object.assign({key: restKey}, item[restKey]));
        }
        return info;
    }

    addTransaction(item: any, cabecera: string) {
        this.fdb.database.ref(cabecera).push().set(item);
    }

    getReservaUID(id) {
        var val = this.fdb.database.ref().child('Reservas').orderByChild('usuario').equalTo(id).once('value').then((objeto) => {
            return objeto.val();
        });
        return from(val);
    }

    getPrimeraReserva(libro: any) {
        var info;
        var val = this.fdb.database.ref().child('Reservas/').orderByChild('libroid').equalTo(libro).once('value').then(async (objeto) => {
            var valor = 0;
            info = _.chain(this.extraerKeyeInfo(objeto.val())).orderBy('fecha', 'asc').value();
            for (let item in info) {
                if (info[item]['estado'] == 'pendiente') {
                    this.fdb.database.ref('Reservas/' + info[item]['key']).update({estado: 'Recoger'});
                    valor = 1;
                    this.event.publish("reservado:activo");
                    break;
                }
            }
            return valor;
        });
        return from(val);
    }

    public alertas = 0;
    getUsuario(key){
        return this.fdb.list('Usuarios/' + key).valueChanges();
    }

    comprobarDisponibilidad(item: any) {
        var val = this.fdb.database.ref().child('Ejemplares/').orderByChild('idlibro').equalTo(item).once('value').then((objeto) => {
            return objeto.val();
        });
        let toast;
        from(val).subscribe(async (objeto) => {
            console.log(objeto);
            var result = 0;
            if( parseInt(global.penalizacion)==0){
                for (let valor in objeto) {
                    if (objeto[valor]['estado'] == 'Disponible' ) {
                        this.getPrimeraReserva(item).subscribe(async (item) => {
                            if (item) {
                                this.fdb.database.ref('Ejemplares/' + valor).update({estado: 'Reservado'});
                                this.alertas = 1;
                                toast = await this.toast.create({
                                    message: 'Puede pasar a recoger el libro.',
                                    duration: 2000
                                });
                                toast.present();
                            }
                        });
                        this.getEjemplaresItems(item).subscribe((rest) => {
                            this.book.ejemplares = this.extraerKeyeInfo(rest);
                            this.book.disponible = this.book.disponible - 1;
                            this.book.reservado = this.book.reservado + 1;
                        });
                        result = 1;
                        break;
                    }
                }
            } else{

                toast = await this.toast.create({
                    message: 'Actualmente estás penalizado y no puedes reservar libros.',
                    duration: 2000
                });
                toast.present();
            }
            if (result != 1) {
                for (let valor in objeto) {
                    if (objeto[valor]['estado'] == 'Prestado') {

                        this.reservarPrestado(valor);
                        this.fdb.database.ref('Ejemplares/' + valor).update({estado: 'Reservado'});
                        toast = await this.toast.create({
                            message: 'Te avisaremos cuando puedas recoger el libro.',
                            duration: 2000
                        });
                        this.getEjemplaresItems(item).subscribe((rest) => {
                            this.book.ejemplares = this.extraerKeyeInfo(rest);
                            this.book.disponible = this.book.prestado - 1;
                            this.book.reservado = this.book.reservado + 1;
                        });
                        toast.present();
                        break;
                    }
                }
            }
            this.event.publish("reservado:desativo");
        });

    }

    reservarPrestado(item) {
        var val = this.fdb.database.ref().child('prestamos/').orderByChild('ejemplarid').equalTo(item).once('value').then((objeto) => {
            return objeto.val();
        });
        from(val).subscribe((objeto) => {
            console.log('llegue aqui.');
            var info = _.chain(this.extraerKeyeInfo(objeto)).orderBy('fechaFin', 'asc').value();
            console.log(info);
            for (let item in info) {
                console.log(item);
                if (info[item]['fechaFin'] == '') {
                    this.fdb.database.ref('prestamos/' + info[item]['key']).update({reservado: 1});
                }
            }
        });

    }

    obteinuser(item: string) {
        var val = this.fdb.database.ref().child('Usuarios/').orderByChild('email').equalTo(item).once('value').then((objeto) => {
            return objeto.val();
        });
        return from(val);
    }

    prestamos() {
        var val = this.fdb.database.ref().child('prestamos/').orderByChild('usuario').equalTo(this.auth.id).once('value').then((objeto) => {
            return objeto.val();
        });
        return from(val);
    }

    renovar(key, renovaciones) {
        return this.fdb.database.ref('prestamos/' + key).update({renovaciones: 1 + renovaciones});
    }

    cancelarReserva(key, estado, libro) {
        if (estado === 'Recoger') {
            this.fdb.database.ref().child('Ejemplares/').orderByChild('idlibro').equalTo(libro).once('value').then((objeto) => {
                for (let item in objeto.val()) {
                    if (objeto.val()[item]['estado'] === 'Reservado') {
                        console.log(objeto.val()[item]);
                        this.fdb.database.ref('Ejemplares/' + item).update({estado: 'Disponible'});
                        break;
                    }
                }
            });

        }
        return this.fdb.database.ref('Reservas/' + key).update({estado: 'Cancelado'});
    }

    getListas() {
        var val = this.fdb.database.ref('Listas/' + this.auth.id).once('value').then((valor) => valor.val());
        return from(val);
    }

    getListaInfo(id: string) {
        var val = this.fdb.database.ref('Listas/' + this.auth.id + '/' + id).once('value').then((valor) => {
            return valor.val();
        });
        return from(val);
    }

    eliminarCampoLista(key){
        return this.fdb.database.ref('Listas/'+this.auth.id+'/' + key).remove();

    }

    getPenalizacion() {

        var val = this.fdb.database.ref('Usuarios/' + this.auth.id ).once('value').then((valor) => {
            return valor.val();
        });
        return from(val);

    }
}


