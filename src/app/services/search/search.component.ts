import {Component, OnDestroy, OnInit} from '@angular/core';
import { Subscription} from 'rxjs';
import * as _ from 'lodash';
import {FirebaseService} from '../firebase.service';
import {Events, LoadingController, NavController} from '@ionic/angular';

import {global} from '../../global';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
    public busqueda: any;
    public searchQuery ="";
    public vacio = false;
    constructor(private fire: FirebaseService, private nav: NavController, public loader: LoadingController,public events: Events) {
    }
    su = new Subscription();
    public gaming ='all';
    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.su.unsubscribe();
    }

    pagina(ruta: any) {
        const loader = this.loader.create({
            message: "Buscando información..."
        });
        loader.then(a=> {
            a.present().then(()=>{
                this.nav.navigateForward(["/book/",ruta]);
                a.dismiss();
            });
        });
    }
    updateList() {
        console.log("Buscando...");
        if(this.searchQuery===""){
            global.busqueda=true;
            this.busqueda=[];
            this.events.publish('user:busqueda');
            return;}
        const loader = this.loader.create({
            message: "Buscando información..."
        });
        loader.then(a=> {
            a.present().then(()=>{
                this.fire.getLibros().subscribe(resp => {
                    console.log("Busqueda:", resp);
                    global.busqueda=false;
                    this.events.publish('user:busqueda');
                    console.log("Filtro",this.gaming);
                    this.busqueda = _.chain(resp)
                        .filter(g=> (this.gaming==="title") ? g.title.toLowerCase().indexOf(this.searchQuery.toLowerCase())!==-1 : ""
                            ||
                            (this.gaming==='author' ) ? g.author.toLowerCase().indexOf(this.searchQuery.toLowerCase())!==-1 : ""
                            ||
                            (this.gaming==='category') ? g.categoria.toLowerCase().indexOf(this.searchQuery.toLowerCase())!==-1 : ""
                            ||
                            (this.gaming==='all') ? g.categoria.toLowerCase().indexOf(this.searchQuery.toLowerCase())!==-1
                                                    ||
                                                    g.author.toLowerCase().indexOf(this.searchQuery.toLowerCase())!==-1
                                                    ||
                                                    g.title.toLowerCase().indexOf(this.searchQuery.toLowerCase())!==-1: ""
                        )
                        .map(g=>{
                            console.log(g);
                                return {
                                    key: g['key'],
                                    title: g['title'],
                                    cate:  g['categoria'],
                                    author:  g['author'],
                                    img:  g['img'],
                                }
                            }
                        ).value();
                    (this.busqueda.length == 0 ) ? this.vacio=true : this.vacio = false;
                    console.log("Filtrado:",this.busqueda);
                });
            });
            a.dismiss();
        });

        console.log("Información:");
    }

}
