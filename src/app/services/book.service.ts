import { Injectable } from '@angular/core';
import {Events} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  public _author;
  private _title;
  get title() {
    return this._title;
  }

  set title(value) {
    this._title = value;
  }

  public _argumento;
  private _reservado;
  public _disponible;
  public _ejemplares;
  public _categoria;
  public _prestado;
  public _description;
  public _edicion;
  private _editorial;
  get author() {
    return this._author;
  }

  get reservado() {
    return this._reservado;
  }

  set reservado(value) {
    this._reservado = value;
    console.log('entre');
    this.event.publish('book:reservado');
  }

  set author(value) {
    this._author = value;
    this.event.publish('book:author');
  }

  get argumento() {
    return this._argumento;
  }


  get editorial() {
    return this._editorial;
  }

  set editorial(value) {
    this._editorial = value;
    this.event.publish('book:editorial');
  }

  set argumento(value) {
    this._argumento = value;
    this.event.publish('book:argumento');
  }

  get disponible()  {
    return this._disponible;
  }

  set disponible(value) {
    this._disponible = value;
    this.event.publish('book:disponible');
  }

  get edicion() {
    return this._edicion;
  }

  set edicion(value) {
    this._edicion = value;
    this.event.publish('book:edicion');
  }

  get ejemplares() {
    return this._ejemplares;
  }

  set ejemplares(value) {
    this._ejemplares = value;
    this.event.publish('book:ejemplares');
  }

  get categoria() {
    return this._categoria;
  }

  set categoria(value) {
    this._categoria = value;
    this.event.publish('book:categoria');
  }

  get prestado() {
    return this._prestado;
  }

  set prestado(value) {
    this._prestado = value;
    this.event.publish('book:prestado');
  }

  get description() {
    return this._description;
  }

  set description(value) {
    this._description = value;
    this.event.publish('book:descrip');
  }

  constructor(private event: Events) { }

}
