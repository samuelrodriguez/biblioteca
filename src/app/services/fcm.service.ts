import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Platform, ToastController} from '@ionic/angular';
import {Firebase} from '@ionic-native/firebase/ngx';
import {AngularFireDatabase} from 'angularfire2/database';
import {AuthenticationService} from './authentication.service';

@Injectable({
    providedIn: 'root'
})
export class FcmService {
    private usuarioLogueado;
    constructor(private firebase: Firebase,
                private auth: AuthenticationService,
                private afs: AngularFirestore,
                private db:AngularFireDatabase,private toastController:ToastController,
                private platform: Platform) {

    }
    private async presentToast(message) {
        const toast = await this.toastController.create({
            message,
            duration: 3000
        });
        toast.present();
    }

    async getToken() {
        let token;

        if (this.platform.is('android')) {
            token = await this.firebase.getToken();
        }
        if (this.platform.is('ios')) {
            token = await this.firebase.getToken();
            await this.firebase.grantPermission();
        }

        this.saveToken(token);
    }

    private saveToken(token) {
        if (!token) {
            return;
        }
        const data = {
            token: token
        };
        console.log("data: ",data);
        if(this.auth.id){
            const realTime= this.db.database.ref('Usuarios/'+this.auth.id).update(data);
        }
        const devicesRef = this.afs.collection('devices');

        return devicesRef.doc(token).update(data);
    }

    onNotifications() {
        return this.firebase.onNotificationOpen();
    }

}
