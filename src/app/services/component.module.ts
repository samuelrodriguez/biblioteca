import { NgModule } from '@angular/core';
import {SearchComponent} from './search/search.component';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NovedadesComponent} from "../novedades/novedades.component";
@NgModule({
    declarations: [SearchComponent,NovedadesComponent],
    imports:[IonicModule,CommonModule,FormsModule],
    exports: [SearchComponent,NovedadesComponent]
})
export class ComponentModule {}
