import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FirebaseService} from '../services/firebase.service';
import {Events, LoadingController, NavController} from '@ionic/angular';
import * as _ from 'lodash';
import {global} from '../global';

@Component({
  selector: 'app-mylists',
  templateUrl: './mylists.page.html',
  styleUrls: ['./mylists.page.scss'],
})
export class MylistsPage implements OnInit {

  public informacion;
  global=global.penalizacion;

  constructor(private events:Events, private def: ChangeDetectorRef,private fire:FirebaseService, public load:LoadingController,public nav: NavController) {

    this.events.subscribe('lista:cancelado',(valor)=>{
      for(let into in this.informacion){

        if(this.informacion[into]['key']===valor){
          this.informacion[into]['number']= (this.informacion[into]['number'] -1);
          break;
        }
      }
      this.def.detectChanges();
    });
  }

  ngOnInit() {
    global.busqueda=true;
    this.events.publish('user:busqueda');
    this.events.subscribe('user:loggedin',()=>{
      this.global=global.penalizacion;
      this.def.detectChanges();
    });

    const loader = this.load.create({
      message: "Cargando listas..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.fire.getListas().subscribe((valor)=>{
          let info =[];
          for (let item in valor){
            info[info.length]={key: item, number: _.countBy(valor[item])["[object Object]"]};
          }
          this.informacion=info;
          a.dismiss();
        });
      });
    });
  }

  abrirLista(item) {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.nav.navigateForward(["/lista-visualizada/",item]);
        a.dismiss();
        this.ngOnInit();
      });
    });
  }

}
