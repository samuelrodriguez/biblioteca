import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Events, LoadingController, Platform, ToastController} from '@ionic/angular';
import {AuthenticationService} from '../services/authentication.service';
import {AngularFireList} from '@angular/fire/database/interfaces';
import {Observable} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database/database';
import {User} from './user';
import {FcmService} from '../services/fcm.service';
import {global} from '../global';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  ref: AngularFireList<any>;
  global=global.penalizacion;
  data: Observable<any[]>;
  transaction = {
    ejemplarid: "53859472113",
    fechaFin:"",
    fechaIni: new Date().setDate(new Date().getDate()-7),
    idlibro: "402173976",
    notificacion: 0,
    renovaciones: 0,
    reservado: 0,
    usuario: "ROGILL6ZsIgF8P9x90DtrybJrPy1",
  };
  user = {} as User;
  checkBox: boolean=true;
  constructor(private events:Events, private def: ChangeDetectorRef,
              private fcm:FcmService,
              private platform: Platform, private authService: AuthenticationService, private db: AngularFireDatabase, private toastController: ToastController, private loader: LoadingController) {
  }

  addTransaction() {
    var objeto: number= Math.trunc(Math.random()*100000000000);
    this.db.database.ref('prestamos/').push(this.transaction);

  }
  async login() {
    if(this.user.email.length===0 || this.user.password.length===0){
      const alert = await this.toastController.create({
        message: 'El usuario o la contraseña estan vacias.',
        duration: 2000
      });
      await alert.present();
    }else{
      const loader = this.loader.create({
        message: "Autentificando..."
      });
      loader.then(a=> {
        a.present().then(()=>{

          const autentic = this.authService.login(this.user,this.checkBox);
          autentic.catch(async e=>{
            const alert = await this.toastController.create({
              message: e.message,
              duration: 2000
            });
            await alert.present();
            if(!e){
              this.events.publish('user:loggedin');
            }
          }).then(()=>{this.fcm.getToken();          a.dismiss();
          });

        });
      });
    }

  }

  ngOnInit() {
    this.events.subscribe('user:loggedin',()=>{
      this.global=global.penalizacion;
      this.def.detectChanges();
    });

  }

  private async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 3000
    });
    toast.present();
  }



  private createCharts(result) {

  }

  private updateCharts(result) {

  }
}
