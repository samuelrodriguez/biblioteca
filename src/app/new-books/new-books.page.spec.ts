import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBooksPage } from './new-books.page';

describe('NewBooksPage', () => {
  let component: NewBooksPage;
  let fixture: ComponentFixture<NewBooksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBooksPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBooksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
