import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CameraService} from '../services/camera.service';
import {FirebaseService} from '../services/firebase.service';
import {Events, ToastController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import {global} from '../global';


@Component({
  selector: 'app-new-books',
  templateUrl: './new-books.page.html',
  styleUrls: ['./new-books.page.scss'],
})
export class NewBooksPage implements OnInit {
  private imagen: boolean=false;
  private imageSrc;
  titulo: string="";
  author: string="";
  isbn: number;
  categoria: string="";
  editorial: string="";
  edicion: string="";
  ejemplares: number=1;
  todo: FormGroup;
  global=global.penalizacion;

  constructor(private events:Events, private def: ChangeDetectorRef,private camera: CameraService,private barcodeScanner: BarcodeScanner,private fire: FirebaseService, private toast: ToastController, private formBuilder: FormBuilder) {
    this.todo = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      isbn: [''],
      categoria: ['', Validators.required],
      editorial: [''],
      edicion: [''],
      ejemplares: ['', Validators.required],

    });
  }

  ngOnInit() {
    global.busqueda=true;
    this.events.publish('user:busqueda');
    this.events.subscribe('user:loggedin',()=>{
      this.global=global.penalizacion;
      this.def.detectChanges();
    });
  }

  codigobarras(){
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.todo.value.isbn=barcodeData.text;
      this.isbn= parseInt(barcodeData.text);
    }).catch(err => {
      console.log('Error', err);
    });
  }

   galery() {
      this.camera.openGallery();
      this.imageSrc= this.camera.imageSrc;
      this.imagen=true;
    }

  async enviar() {
    let toat= await this.toast.create({
      message: "Enviada solicitud de libro.",
      duration:3000
    });
    toat.present();
    const transaccion={
      titulo: this.todo.value.title,
      author: this.todo.value.author,
      isbn: (this.todo.value.isbn != undefined)? this.todo.value.isbn: 0,
      categoria: this.todo.value.categoria,
      editorial: this.todo.value.editorial,
      edicion: this.todo.value.edicion,
      ejemplares: this.todo.value.ejemplares
    };
    this.fire.addTransaction(transaccion,"Desideratas");
    this.todo.reset();
  }

  logForm() {
    console.log(this.todo);
    console.log(this.todo.value.title);
  }
}
