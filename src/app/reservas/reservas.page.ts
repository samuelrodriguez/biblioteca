import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FirebaseService} from '../services/firebase.service';
import {AuthenticationService} from '../services/authentication.service';
import {AlertController, Events, LoadingController, NavController} from '@ionic/angular';
import {Observable} from 'rxjs';
import * as _ from 'lodash';
import {global} from '../global';

@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.page.html',
  styleUrls: ['./reservas.page.scss'],
})
export class ReservasPage implements OnInit {
  public reservas: any;
  global=global.penalizacion;

  constructor(private events:Events, private def: ChangeDetectorRef,public fire: FirebaseService,private alertController:AlertController, public auth: AuthenticationService,public load: LoadingController, private nav: NavController) { }

  ngOnInit() {
    this.events.subscribe('user:loggedin',()=>{
      this.global=global.penalizacion;
      this.def.detectChanges();
    });
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.fire.getReservaUID(this.auth.id).subscribe((objeto)=>{
          for(let item in objeto){
            this.fire.getLibro(objeto[item]['libroid']).subscribe((libro)=>{

              this.fire.getReservaUID(this.auth.id).subscribe((reserva)=>{
                for(let valor in reserva){
                  if(reserva[valor]['libroid']=== objeto[item]['libroid']){
                    objeto[item]=Object.assign(objeto[item],{idreserva: item,idlibro: reserva[valor]['libroid'],author: libro[2],titulo:libro[6],img:libro[5],categoria:libro[3]});
                    this.reservas=_.chain(objeto).orderBy('estado','desc').filter(g=> g.estado!="Entregado" &&  g.estado!="Cancelado").map(g=>{
                      return {
                        estado: g.estado,
                        autor: g.author,
                        idreserva: g.idreserva,
                        titulo: g.titulo,
                        idlibro: g.idlibro,
                        img: g.img,
                        categoria: g.categoria,
                        fecha: g.fecha,
                      }
                    }).value();
                    console.log(this.reservas);
                  }
                }
              });
            });
          }
        }); a.dismiss();
      });
    });
  }

  pagina(ruta: any) {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.nav.navigateForward(["/book/",ruta]);
        a.dismiss();
      });
    });
  }
  async cancelarReserva(reserva: string, titulo: string,estado,libro) {

    const alert = await this.alertController.create({
      message: "¿Está seguro de que quiere cancelar esta reserva?"+ "<div style='font-weight: bold'>"+titulo+"</div>",
      buttons:['Cerrar', {text:'Aceptar',handler:()=>{this.fire.cancelarReserva(reserva,estado,libro);this.ngOnInit()}}]
    });
    await alert.present();
  }

}
