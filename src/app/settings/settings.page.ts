import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {global} from '../global';
import {Events} from '@ionic/angular';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  global=global.penalizacion;

  constructor(private events:Events, private def: ChangeDetectorRef) { }

  ngOnInit() {
    global.busqueda=true;
    this.events.publish('user:busqueda');
    this.events.subscribe('user:loggedin',()=>{
      this.global=global.penalizacion;
      this.def.detectChanges();
    });
  }

}
