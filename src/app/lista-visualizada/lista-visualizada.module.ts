import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListaVisualizadaPage } from './lista-visualizada.page';

const routes: Routes = [
  {
    path: '',
    component: ListaVisualizadaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListaVisualizadaPage]
})
export class ListaVisualizadaPageModule {}
