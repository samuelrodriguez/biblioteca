import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaVisualizadaPage } from './lista-visualizada.page';

describe('ListaVisualizadaPage', () => {
  let component: ListaVisualizadaPage;
  let fixture: ComponentFixture<ListaVisualizadaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaVisualizadaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaVisualizadaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
