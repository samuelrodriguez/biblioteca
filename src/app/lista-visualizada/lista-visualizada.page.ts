import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as _ from 'lodash';
import {FirebaseService} from '../services/firebase.service';
import {Events, LoadingController, NavController} from '@ionic/angular';
import {global} from '../global';

@Component({
  selector: 'app-lista-visualizada',
  templateUrl: './lista-visualizada.page.html',
  styleUrls: ['./lista-visualizada.page.scss'],
})
export class ListaVisualizadaPage implements OnInit {

  argumento;
  informacion;
  global=global.penalizacion;

  constructor(private events:Events, private def: ChangeDetectorRef,private activeRoute: ActivatedRoute,private nav:NavController,private load:LoadingController, private fire: FirebaseService) {

    this.argumento=this.activeRoute.snapshot.paramMap.get('id');

  }

  ngOnInit() {
    this.events.subscribe('user:loggedin',()=>{
      this.global=global.penalizacion;
      this.def.detectChanges();
    });

    this.fire.getListaInfo(this.activeRoute.snapshot.paramMap.get('id')).subscribe((objeto)=>{
      let info;
      for(let item in objeto){
        this.fire.getLibro(objeto[item]['libroid']).subscribe((value)=>{
          objeto[item]=Object.assign(value,{identificador: item, key:objeto[item]['libroid']});
          this.informacion=_.chain(objeto).map(g=>{return {img:g[5], titulo: g[6], key: g.key, identificador: g.identificador}}).value();

        });
      }
    });
  }

  elimintar(key){
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.fire.eliminarCampoLista(this.argumento+"/"+key);
        a.dismiss();
        this.ngOnInit();
      });
    });
    this.events.publish('lista:cancelado',this.argumento);
  }
  pagina(ruta: any) {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.nav.navigateForward(["/book/",ruta]);
        a.dismiss();
      });
    });
  }
}
