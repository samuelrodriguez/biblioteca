import {Component, OnInit, ViewChild} from '@angular/core';
import {FirebaseService} from '../services/firebase.service';
import {LoadingController, NavController} from '@ionic/angular';


@Component({
  selector: 'app-novedades',
  templateUrl: './novedades.component.html',
  styleUrls: ['./novedades.component.scss']
})
export class NovedadesComponent implements OnInit {
  novedades=[];

  slideOpts = {
    slidesPerView: 4,
    initialSlide: 0,
    speed: 400
  };

  constructor(private fdb: FirebaseService, private nav: NavController, private load: LoadingController) { }

  ngOnInit() {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.fdb.getLibros().subscribe((objeto)=>{
          let info= this.valores(objeto);
          for(let i =0;i<6;i++){
            this.novedades[i]=objeto[info[i]];
          }
          a.dismiss();
        });
      });
    });
  }

  valores(objeto){
    let array=[];
    let k=0;
    while(k<6){
      let valor=this.randomIntFromInterval(0,objeto.length-1);
      if( !array.includes(valor) ){
        array[k]=valor;
        k++;
      }
    }
    return array;

  }

  randomIntFromInterval(min,max) // min and max included
  {
    return Math.floor(Math.random()*(max-min+1)+min);
  }


  pagina(ruta: any) {
    const loader = this.load.create({
      message: "Buscando información..."
    });
    loader.then(a=> {
      a.present().then(()=>{
        this.nav.navigateForward(["/book/",ruta]);
        a.dismiss();
      });
    });
  }
}
