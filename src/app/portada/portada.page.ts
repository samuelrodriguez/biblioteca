import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {global} from '../global';
import {Events} from '@ionic/angular';
import {FirebaseService} from '../services/firebase.service';
import {AngularFireDatabase} from '@angular/fire/database';

@Component({
    selector: 'app-portada',
    templateUrl: './portada.page.html',
    styleUrls: ['./portada.page.scss'],
})
export class PortadaPage implements OnInit {
    global=global;

    constructor(private events:Events,private db:AngularFireDatabase, private def: ChangeDetectorRef) {
        global.busqueda=true;
        this.global=global;
    }
    ngOnInit(): void {
        this.events.subscribe('user:loggedin',()=>{
            this.global=global;
            this.def.detectChanges();
        });

        this.events.subscribe('user:busqueda',()=>{
            this.global=global;
        });
    }

    libros() {
        /**/
        let transaction = {
            Edicion: '5º Edición 2011',
            Editorial: 'MARCIAL PONS',
            author: 'NEIL MACCORMICK',
            categoria: 'Poesía española.',
            description: "Instituciones del Derecho contiene una presentación con pretensiones de exhaustividad de una teoría institucional del Derecho. Pero, a diferencia de buena parte de la literatura contemporánea sobre la materoa, es un libro con el cual el lector puede también aprender de Derecho. Aunque comienza ofreciendo una definición de Derecho como un orden normativo institucional, luego muestra cómo desde esa perspectiva es posible arrojar luz sobre cuestiones fundamentales del Derecho público y del Derecho privado. En esto el libro es un libro de \"instituciones\" en un sentido que lo vincula a una forma tradicional de literatura jurídica, dela cuál las instituciones del Derecho de Escocia (1681) de Lord Stair son un ejemplo paradigmático.",
            img: "https://firebasestorage.googleapis.com/v0/b/biblioteca-82033.appspot.com/o/9788497688598.jpg?alt=media&token=03fa6b28-54f3-41e9-8a8b-54154e957692",
            title:'INSTITUCIONES DEL DERECHO',
        };
        var objeto: number= Math.trunc(Math.random()*1000000000);
        console.log(objeto);
        this.db.database.ref('Libros/'+objeto).set(transaction);
        /*
              let transaction = {
                estado: "Disponible",
                idlibro:"244103928",
              };
              var objeto: number= Math.trunc(Math.random()*100000000000);
              this.db.database.ref('Ejemplares/'+objeto).set(transaction);
              */
    }
}
