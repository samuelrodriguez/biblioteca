import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PortadaPage } from './portada.page';
import {PenalizacionComponent} from '../penalizacion/penalizacion.component';
import {RenovarComponent} from '../renovar/renovar.component';
import {ReservasComponent} from '../reservas/reservas.component';
import {ViewListComponent} from '../view-list/view-list.component';
import {ComponentModule} from '../services/component.module';
import {NovedadesComponent} from '../novedades/novedades.component';

const routes: Routes = [
  {
    path: '',
    component: PortadaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PortadaPage,PenalizacionComponent,RenovarComponent,ReservasComponent,ViewListComponent]
})
export class PortadaPageModule {}
